# Ingenieria de software 2 Demo - React

## Requerimientos
* node `^5.0.0`
* yarn `^0.23.0` o npm `^3.0.0`

## Instalación

    bash
    $ git clone git@bitbucket.org:mgoffan/inge2-demo.git
    $ cd inge2-demo
    $ yarn
    $ yarn start


Acceder a: `http://localhost:3000`