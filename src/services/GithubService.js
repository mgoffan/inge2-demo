import superagent from 'superagent';

export const search = (q) => {
	return new Promise((resolve, reject) => {
		superagent
		.get('https://api.github.com/search/repositories')
		.set('Accept', 'application/json')
		.query({ q })
		.end((err, res) => {
			if (err) {
				return reject(err);
			}
			if (res.status >= 400) {
				return resolve(res.body);
			}
			return resolve(res.body.items);
		})
	});
}