import constants from 'namespace-constants';
export default constants('github', [
	'fetching',
	'fetched'
], {
	separator: '/',
	transform: function (v) {
		return v.replace(/\ /g, '_').toUpperCase();
	}
});