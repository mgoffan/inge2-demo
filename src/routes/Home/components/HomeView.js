import React from 'react';
import { connect } from 'react-redux';
import DuckImage from '../assets/Duck.jpg';
import { fetch } from '../../../actions/github';

import _ from 'lodash';

import './HomeView.scss'

const letters = '0123456789ABCDEF';

const randomColor = () => _.times(6, i => letters[Math.floor(Math.random() * letters.length)]).join('');

export class HomeView extends React.Component {

	constructor(props) {
		super(props);
		this.fetchFromGithub = _.debounce(this.fetchFromGithub.bind(this), 400);
	}

	componentWillMount() {

	}

	fetchFromGithub() {
		this.props.fetch(this.query.value);
	}

	onFormChange() {
		this.fetchFromGithub();
	}

	render() {
		return (
			<div>
	    	<h4>Bienvenidos!</h4>
	    	<form>
	    		<input
	    			className="form-control"
	    			onChange={e => {
	    				this.onFormChange();
	    			}}
	    			placeholder="Busqueda"
	    			ref={ref => this.query = ref} />
	    	</form>
	    	
  
	    	{_.map(this.props.repos, repo => {
	    		return (
	    			<div className="media">
	    				<div className="media-left media-middle">
		    				<a href="#">
		    					<img className="media-object" src={`http://via.placeholder.com/60/${randomColor()}/${randomColor()}?text=${repo.name.substr(0, 2).toUpperCase()}`} alt="..." />
		    				</a>
		    			</div>
		    			<div className="media-body">
		    				<h4 className="text-left media-heading">{repo.name}</h4>
		    				<p className="text-left"><b>{repo.language}</b> {repo.description}</p>
		    			</div>
		    		</div>
	    			)
	    	})}
	  	</div>
	  )
	}
}

const mapStateToProps = state => ({
	repos: state.github.get('repos').toJS()
});

const mapDispatchToProps = dispatch => ({
	fetch: v => dispatch(fetch(v))
})

export default connect(mapStateToProps, mapDispatchToProps)(HomeView);