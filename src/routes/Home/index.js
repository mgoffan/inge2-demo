// import HomeView from './components/HomeView';
import { injectReducer } from '../../store/reducers';

// Sync route definition
export default (store) => {
	const reducer = require('./reducer').default;
	const HomeView = require('./components/HomeView').default;

  /*  Add the reducer to the store on key 'counter'  */
  injectReducer(store, { key: 'github', reducer })
  return HomeView;
}

