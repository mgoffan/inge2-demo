import C from '../../constants/github';
import { Map, List } from 'immutable';

const handlers = {
	[C.FETCHING] : (state, payload) => {
		return state.set('loading', true);
	},

	[C.FETCHED] : (state, payload) => {
		return state.merge({
			loading: false,
			repos: List(payload)
		});
	}
};

const initialState = Map({
	loading: false,
	repos: List()
})
export default (state = initialState, action) => {
	const handler = handlers[action.type];

	return handler ? handler(state, action.payload) : state;
}