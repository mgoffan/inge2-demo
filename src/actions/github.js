import C from '../constants/github';
import { search } from '../services/GithubService';

export const fetching = () => ({
	type: C.FETCHING
});

export const fetched = (payload) => ({
	type: C.FETCHED,
	payload
});

export const fetch = (query) => (dispatch, getState) => {

	dispatch(fetching());

	return search(query).then(results => {


		return dispatch(fetched(results));

	}, err => {

	})
}